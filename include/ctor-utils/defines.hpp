#ifndef HEAER_CTORUTILS_DEFINES
#define HEAER_CTORUTILS_DEFINES 1

// // //
// declarations

#define CTOR_DECL_1(type, impl) \
type() impl

#define CTOR_DECL_1_FWD(type) CTOR_DECL_1(type, ;)



#define CTOR_DECL_VD(type) \
virtual ~type() = default

#define CTOR_DECL_DTOR(type, impl) \
~type() impl

#define CTOR_DECL_DTOR_FWD(type) CTOR_DECL_DTOR(type, ;)



#define CTOR_DECL_COPY(type, impl) \
type(const type &) impl; \
type & operator=(const type &) impl

#define CTOR_DECL_COPY_FWD(type) CTOR_DECL_COPY(type, ;)
#define CTOR_DECL_COPY_DFL(type) CTOR_DECL_COPY(type, = default)
#define CTOR_DECL_COPY_DEL(type) CTOR_DECL_COPY(type, = delete)



#define CTOR_DECL_MOVE(type, impl) \
type(type &&) impl; \
type & operator=(type &&) impl

#define CTOR_DECL_MOVE_FWD(type) CTOR_DECL_MOVE(type, ;)
#define CTOR_DECL_MOVE_DFL(type) CTOR_DECL_MOVE(type, = default)
#define CTOR_DECL_MOVE_DEL(type) CTOR_DECL_MOVE(type, = delete)



#define CTOR_DECL_4(type, impl) \
CTOR_DECL_COPY(type, impl); \
CTOR_DECL_MOVE(type, impl)

#define CTOR_DECL_4_FWD(type) CTOR_DECL_4(type, ;)
#define CTOR_DECL_4_DFL(type) CTOR_DECL_4(type, = default)
#define CTOR_DECL_4_DEL(type) CTOR_DECL_4(type, = delete)



#define CTOR_DECL_5(type, impl) \
CTOR_DECL_1(type, impl); \
CTOR_DECL_4(type, impl)

#define CTOR_DECL_5_FWD(type) CTOR_DECL_5(type, ;)
#define CTOR_DECL_5_DFL(type) CTOR_DECL_5(type, = default)
#define CTOR_DECL_5_DEL(type) CTOR_DECL_5(type, = delete)



#define CTOR_DECL_VIFACE(type, visibility) \
visibility: CTOR_DECL_5(type, = default); \
public: CTOR_DECL_VD(type)

#define CTOR_DECL_VIFACE_FWD(type, visibility) \
visibility: CTOR_DECL_5_FWD(type); \
public: virtual ~type()




// // //
// implementation

#define CTOR_IMPL_1(type, impl) \
type::type() impl

#define CTOR_IMPL_DTOR(type, impl) \
type::~type() impl

#define CTOR_IMPL_COPY(type, impl) \
type::type(const type &) impl; \
type & type::operator=(const type &) impl

#define CTOR_IMPL_MOVE(type, impl) \
type::type(type &&) impl; \
type & type::operator=(type &&) impl

#define CTOR_IMPL_4(type, impl) \
CTOR_IMPL_COPY(type, impl); \
CTOR_IMPL_MOVE(type, impl)

#define CTOR_IMPL_5(type, impl) \
CTOR_IMPL_1(type, impl); \
CTOR_IMPL_4(type, impl)

#define CTOR_IMPL_VIFACE(type) \
CTOR_IMPL_5(type, = default); \
type::~type() = default

#endif
